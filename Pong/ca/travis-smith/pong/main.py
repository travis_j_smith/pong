import pygame;
from player import *;
from ball import Ball;

import os

from pygame.locals import QUIT
from pygame.locals import K_UP
from pygame.locals import K_DOWN
from pygame.locals import K_w
from pygame.locals import K_s
from pygame.locals import KEYDOWN

class Game:
  _white = (255, 255, 255)

  def Initialize(self):
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pygame.init()
    self.clock = pygame.time.Clock()
    self.screen = pygame.display.set_mode((800, 500))
    pygame.display.set_caption('Smitty-Pong')
    pygame.mouse.set_visible(0)
    self.player1 = Player("Player 1", 30, False)
    self.player2 = Player("Player 2", 760, False)
    self.ball = Ball(400, 225)
    self.Blackout()
    
  def DrawGame(self):
    #top bar
    pygame.draw.rect(self.screen, self._white, (0, 20, 800, 10), 0)
    #bottom bar
    pygame.draw.rect(self.screen, self._white, (0, 470, 800, 10), 0)
    #centre dots
    for i in range(0, 220, 10):
      pygame.draw.rect(self.screen, self._white, (400, (i * 2 + 35), 10, 10), 0)
      
  def DrawPlayers(self):
    pygame.draw.rect(self.screen, self._white, (self.player1.x, self.player1.y, self.player1.width, self.player1.height), 0)
    pygame.draw.rect(self.screen, self._white, (self.player2.x, self.player2.y, self.player2.width, self.player2.height), 0)
    
  def DrawBall(self):
    pygame.draw.rect(self.screen, self._white, (self.ball.x, self.ball.y, self.ball.width, self.ball.height), 0)
    
  def DrawScore(self):
    myFont = pygame.font.SysFont("monospace", 60, True)
    p1score = myFont.render(str(self.player1.score), 1, self._white)
    self.screen.blit(p1score, (300, 60))
    
    p2score = myFont.render(str(self.player2.score), 1, self._white)
    self.screen.blit(p2score, (465, 60))
    
  #fills the entire screen with a black background
  def Blackout(self):
    background = pygame.Surface(self.screen.get_size())
    background = background.convert()
    background.fill((0, 0, 0))
    self.screen.blit(background, (0, 0))

  def Run(self):
    while True:
      self.clock.tick(60)
      pygame.display.update()
      
      keys = pygame.key.get_pressed()
      if keys[K_w]:
        if (self.player1.y - self.player1.step_length > 30):
          self.player1.Move(Direction.UP)
      elif keys[K_s]:
        if (self.player1.y + self.player1.step_length + self.player1.height < 470):
          self.player1.Move(Direction.DOWN)
        
      if(not self.player2.computer):
        if keys[K_UP]:
          if (self.player2.y - self.player2.step_length > 30):
            self.player2.Move(Direction.UP)
        elif keys[K_DOWN]:
          if (self.player2.y + self.player2.step_length + self.player2.height < 470):
            self.player2.Move(Direction.DOWN)
            
      self.ball.Move(self.player1, self.player2)
      
      for event in pygame.event.get():
        if event.type == QUIT:
          return
      
      self.Blackout()
      self.DrawGame()
      self.DrawPlayers()
      self.DrawBall()
      self.DrawScore()


pongGame = Game()
pongGame.Initialize()
pongGame.Run()