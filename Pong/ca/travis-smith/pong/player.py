from direction import Direction;

class Player:
    
  def __init__(self, name, xloc, computer):
    self.y = 200
    self.width = 10
    self.height = 50
    self.step_length = 4
    self.name = name
    self.x = xloc
    self.computer = computer
    self.score = 0
    
    
  def Move(self, direction):
    if(direction == Direction.UP):
      self.y -= self.step_length
    else:
      self.y += self.step_length
      
  