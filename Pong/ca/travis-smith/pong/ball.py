'''
Created on Jun 5, 2014

@author: smitty
'''
from direction import Direction;
import random;

class Ball:
    
  def __init__(self, x, y):
      self.initial_x = x
      self.initial_y = y
      self.x = x
      self.y = y
      self.width = 10
      self.height = 10
      self.speed = 8
      self.xvelocity = 1
      self.yvelocity = 1
      self.angle = 0
      
  def Move(self, player1, player2):
    #it hit either the top or the bottom, send it back!
    if(self.y <= 30 or (self.y + self.height) >= 470):
        self.yvelocity *= -1
    
    #only change the angle if it  is > 0 -- saves from a division by zero error
    if(self.angle > 0):
        self.y += ((self.speed * self.yvelocity) * self.angle / 15)
    
    #it got past player 1 -- give player 2 a point and reset the ball
    if(self.x + self.width < player1.x):
        self.xvelocity *= -1
        self.x = self.initial_x
        self.y = self.initial_y
        self.angle = 0
        player2.score += 1
    #it got past player 2 -- give player 1 a point and reset the ball
    elif(self.x - self.width > player2.x):
        self.xvelocity *= -1
        self.x = self.initial_x
        self.y = self.initial_y
        self.angle = 0
        player1.score += 1
    #it hit the paddle! Send it back!
    elif(self.x < (player1.x + player1.width) and (self.y + self.height) >= player1.y and self.y <= (player1.y + player1.height)):
        self.xvelocity *= -1
        self.angle = abs(((player1.y + (player1.height / 2) - self.y) / player1.height / 2) * 75)
        print("P1 hit Angle: " + str(self.angle))
    elif((self.x + self.width) > player2.x and (self.y + self.height) >= player2.y and self.y <= (player2.y + player2.height)):
        self.xvelocity *= -1
        self.angle = abs(((player2.y + (player2.height / 2) - self.y) / player2.height / 2) * 75)
        print("P2 hit Angle: " + str(self.angle))
        
    self.x += self.speed * self.xvelocity
